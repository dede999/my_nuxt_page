# CHANGELOG

### This is where I'll report the additions made to the code base. More than this, here's where I may talk about the things I've learned while implementing these things

### Most recent changes come first

- Create a class to manage translations
- Create I18n config for the application
- Change the stylesheet dialect to sass
- Setup for unit testing (issue #6)
  - Config files must be in JS or in JSON (using TS in the config may lead to deep trouble)
  - Test files may be place in a separate directory
    - It applies even more to NextJS, where if you place a test file inside pages folder, you'll end up with a `spec/something` route
    - More than this, it seems a good practice to have an specific folder just for spec file.
- API routes for programming languages model (issue #2)
- Create an abstract class for first use with de DB (issue #1)
  - The next intended use is with validators.
