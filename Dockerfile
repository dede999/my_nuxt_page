FROM node:16.13.0

RUN apt-get update -y && apt-get install -y yarn texlive texlive-extra-utils latexmk texlive-lang-portuguese \
                                                  texlive-latex-extra texlive-fonts-extra

WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn install

COPY . .

RUN yarn build

EXPOSE 3000

CMD ["yarn", "start"]
