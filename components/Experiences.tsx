import { Button, Center, Heading, SimpleGrid, Text } from "@chakra-ui/react"
import { useRouter } from "next/router"
import I18n from "../models/support/i18n"

export default function Experiences() {
  function formatData(data: Date): String {
    return `${data.getDate()}-${data.getMonth() + 1}-${data.getFullYear()}`
  }

  const fakeData = {
    company: "An experience",
    begin: formatData(new Date(2020, 8, 3)),
    description:
      "Qui quo ea velit omnis minima. Id voluptatem veniam et. " +
      "Nam cumque ipsa quas eveniet."
  }

  const { locale } = useRouter()
  const t = I18n.fetch_translation(locale, "components", "experiences")

  return (
    <div id="experience">
      <Heading
        id="experiences"
        className={"divider"}
        fontSize={{ base: "2xl", lg: "4xl", xl: "5xl" }}
      >
        <span> {t.title} </span>
      </Heading>
      <SimpleGrid columns={{ base: 1, md: 2 }}>
        <Center className={"glass"}>
          <SimpleGrid columns={1}>
            <Text
              fontSize={{ base: "lg", lg: "xl", xl: "2xl" }}
              style={{ marginBottom: ".5em" }}
            >
              {t.description}
            </Text>
            <Center>
              <Button style={{ width: "fit-content" }} variant="ghost">
                {t.link}
              </Button>
            </Center>
          </SimpleGrid>
        </Center>
        <Center className="exp_card">
          <SimpleGrid columns={1}>
            <SimpleGrid columns={2}>
              <Heading fontSize={{ base: "lg", lg: "2xl", xl: "3xl" }}>
                {fakeData.company}
              </Heading>
              <Text
                fontSize={{ base: "md", lg: "xl", xl: "3xl" }}
                style={{ backgroundColor: "inherit" }}
                id="begin_work"
              >
                Desde {fakeData.begin}
              </Text>
            </SimpleGrid>
            <Text
              fontSize={{ base: "md", lg: "xl", xl: "2xl" }}
              isTruncated
              noOfLines={5}
            >
              {fakeData.description}
            </Text>
          </SimpleGrid>
        </Center>
      </SimpleGrid>
    </div>
  )
}
