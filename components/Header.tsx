import React from "react"
import { Heading, Center, Button, SimpleGrid } from "@chakra-ui/react"
import { LockIcon } from "@chakra-ui/icons"

export default function Header() {
  return (
    <nav>
      <SimpleGrid
        columns={2}
        style={{
          padding: ".25em",
          justifyContent: "space-around"
        }}
      >
        <Center style={{ backgroundColor: "transparent" }}>
          <Heading
            className={"devName"}
            as={"h1"}
            fontSize={{ base: "2xl", lg: "5xl", xl: "5xl" }}
          >
            André Luiz
          </Heading>
        </Center>
        <Center style={{ backgroundColor: "transparent" }}>
          <Button
            colorScheme="teal"
            variant="outline"
            size={"sm"}
            style={{ marginTop: ".4em" }}
          >
            <LockIcon style={{ backgroundColor: "transparent" }} />
          </Button>
        </Center>
      </SimpleGrid>
    </nav>
  )
}
