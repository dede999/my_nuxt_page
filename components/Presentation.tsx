import {
  SimpleGrid,
  Image,
  Heading,
  Text,
  Container,
  Center
} from "@chakra-ui/react"
import { useRouter } from "next/router"
import I18n from "../models/support/i18n"

export default function Presentation() {
  const pageStyle = {
    container: {
      backgroundColor: "hsla(240, 35%, 0%, .2)",
      paddingBottom: "10px",
      textAlign: "center"
    },
    transparency: {
      backgroundColor: "transparent"
    },
    bgStyle: {
      backgroundColor: "hsla(240, 35%, 0%, .2)"
    }
  }
  const { transparency, bgStyle } = pageStyle
  const { locale } = useRouter()
  const t = I18n.fetch_translation(locale, "components", "presentation")

  return (
    <Container
      centerContent
      maxW={["80%", "65%"]}
      // @ts-ignore
      style={pageStyle.container}
      heigth={"320px"}
    >
      <Heading
        fontSize={{ base: "2xl", lg: "4xl", xl: "5xl" }}
        style={{ backgroundColor: "transparent" }}
      >
        {t.title}
      </Heading>
      <SimpleGrid columns={[1, 2]} alignContent={"center"}>
        <Center style={bgStyle}>
          <Image src="/me.png" boxSize={{ base: 150, xl: 250 }} />
        </Center>
        <Center style={bgStyle}>
          <Text
            fontSize={{ base: "lg", lg: "xl", xl: "2xl" }}
            style={transparency}
          >
            {t.text}
          </Text>
        </Center>
      </SimpleGrid>
    </Container>
  )
}
