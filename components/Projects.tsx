import { useRouter } from "next/router"
import { FaVuejs } from "react-icons/fa"
import I18n from "../models/support/i18n"
import { SiTypescript, SiReact } from "react-icons/si"
import { DiNodejsSmall, DiRubyRough, DiRor } from "react-icons/di"
import { Button, Center, Heading, SimpleGrid, Text } from "@chakra-ui/react"

export default function Projects() {
  const { locale } = useRouter()
  const t = I18n.fetch_translation(locale, "components", "projects")

  return (
    <div id="experience">
      <Heading
        className={"divider"}
        id="projetos"
        fontSize={{ base: "2xl", lg: "4xl", xl: "5xl" }}
      >
        <span> {t.title} </span>
      </Heading>
      <SimpleGrid columns={{ base: 1, md: 2 }}>
        <Center>
          <SimpleGrid columns={[2, 3]} style={{ width: "100%" }}>
            <Center className="tool-ball red">
              <DiRubyRough />
            </Center>
            <Center className="tool-ball red">
              <DiRor />
            </Center>
            <Center className="tool-ball green">
              <DiNodejsSmall />
            </Center>
            <Center className="tool-ball blue">
              <SiTypescript />
            </Center>
            <Center className="tool-ball blue">
              <SiReact />
            </Center>
            <Center className="tool-ball green">
              <FaVuejs />
            </Center>
          </SimpleGrid>
        </Center>
        <Center className={"glass"}>
          <SimpleGrid columns={1}>
            <Text
              fontSize={{ base: "lg", lg: "xl", xl: "2xl" }}
              style={{ marginBottom: ".5em" }}
            >
              {t.text}
            </Text>
            <Center>
              <Button style={{ width: "fit-content" }} variant="ghost">
                {t.link}
              </Button>
            </Center>
          </SimpleGrid>
        </Center>
      </SimpleGrid>
    </div>
  )
}
