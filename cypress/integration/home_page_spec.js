describe("Checking Home Page components", () => {
  beforeEach(() => cy.visit("/"))

  it("is possible to see the navbar and its contents", () => {
    const navBar = cy.get("nav")

    navBar.should("have.text", "André Luiz")
  })

  it("is expected to have an about me section", () => {
    const aboutMe = cy.get("div.chakra-container")

    aboutMe.should("contain.text", "About me")
  })

  it("is expected to have an experiences section", () => {
    const aboutMe = cy.get("h2#experiences")

    aboutMe.should("contain.text", "Experiences")
  })

  it("it is expected to have a project section", () => {
    const project_section = cy.get("h2#projetos")

    project_section.should("contain.text", "Projects")
  })
})
