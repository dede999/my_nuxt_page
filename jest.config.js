module.exports = {
  verbose: true,
  testPathIgnorePatterns: ["<rootDir>/.next/", "<rootDir>/node_modules/"],
  coveragePathIgnorePatterns: [
    "<rootDir>/pages/_app.tsx",
    "<rootDir>/models/support/active-model.ts"
  ],
  bail: 1,
  clearMocks: true,
  collectCoverage: true,
  collectCoverageFrom: [
    "components/**/*.tsx",
    "pages/**/*.tsx",
    "models/**/*.model.ts",
    "models/support/*.ts",
    "modules/*"
  ],
  coverageReporters: ["lcov", "text"],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100
    }
  },
  rootDir: ".",
  setupFilesAfterEnv: ["./setupTests.js"]
}
