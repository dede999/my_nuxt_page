export const eng_components = {
  presentation: {
    title: "About me",
    text:
      "Graduated in Computer Science (July 2020) and very willing to" +
      " go through challenges and learn new technologies"
  },
  experiences: {
    title: "Experiences",
    description:
      "This is where you can check the places I've worked and what I've " +
      "done on each of these places. On the right, it's my last (or ongoing) " +
      "working experience. For further details, check the link below",
    link: "Check my work places"
  },
  projects: {
    title: "Projects",
    text:
      "In this section, you will be able to see the other projects I worked " +
      "on. Filter them by name, frameworks or other criteria.",
    link: "Check my previous projects"
  }
}
