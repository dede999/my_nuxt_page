export const pt_components = {
  presentation: {
    title: "Sobre mim",
    text:
      "Formado em Ciências da Computação (julho 2020) e com bastante" +
      " vontade de fazer e aprender coisas novas e desafiadoras."
  },
  experiences: {
    title: "Experiências",
    description:
      "Aqui você pode ver onde eu trabalhei e o que eu fiz " +
      "em cada lugar. Ao lado, a minha última experiência " +
      "de trabalho (em curso ou não). Para mais detalhes, " +
      "acesse o link abaixo.",
    link: "Veja aonde trabalhei"
  },
  projects: {
    title: "Projetos",
    text:
      "Nesta sessão, você poderá ver os projetos em que trabalhei. " +
      "Filtre-os por nome, ferramentas utilizadas e tantos outros " +
      "critérios",
    link: "Veja os meus projetos"
  }
}
