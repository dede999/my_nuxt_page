export interface Tool {
  create: {
    name: string
  }[]
}

export interface ProgrammingLanguage {
  id?: number
  name?: string
  level?: number
  tools?: Tool
}

export interface ProgrammingLanguageCreation extends ProgrammingLanguage {
  name: string
}

export interface ProgrammingLanguageUpdate {
  name?: string | { set: string }
  level?:
    | number
    | {
        set?: number
        increment?: number
        decrement?: number
        multiply?: number
        divide?: number
      }
  tools: any
}
