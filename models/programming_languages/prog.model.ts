import { PrismaClient } from "@prisma/client"
import { ActiveModel } from "../support/active-model"
import {
  ProgrammingLanguage,
  ProgrammingLanguageCreation,
  ProgrammingLanguageUpdate
} from "./prog.interface"

export class Programming extends ActiveModel {
  public static readonly repository = new PrismaClient().programming_languages

  private static readonly collection = {
    GET: async () => {
      return await Programming.repository.findMany({
        include: { tools: true }
      })
    },
    POST: async (data: ProgrammingLanguageCreation) => {
      return await Programming.repository.create({
        include: { tools: true },
        data
      })
    }
  }

  private static readonly instance = {
    GET: async (_: any, id: number) => {
      return await Programming.repository.findUnique({
        where: { id },
        include: { tools: true }
      })
    },
    PUT: async (data: ProgrammingLanguageUpdate, id: number) => {
      return await Programming.repository.update({
        where: { id },
        include: { tools: true },
        data
      })
    },
    DELETE: async (_: any, name: string) => {
      return await Programming.repository.deleteMany({
        where: { name }
      })
    }
  }

  public static request = async (
    type: "collection" | "instance",
    method: string,
    params: { id?: number; data?: any }
  ): Promise<ProgrammingLanguage> => {
    const { id, data } = params
    return Programming[type][method](data, id)
  }
}
