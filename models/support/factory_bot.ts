export default class FactoryBot<Entity> {
  constructor(private factoryFunction: (...args: any[]) => Entity) {}

  getOneInstance(): Entity {
    return this.factoryFunction()
  }

  getInstances(quantity: number = 1): Entity[] {
    const returned = []
    for (let index = 0; index < quantity; index++) {
      returned[index] = this.getOneInstance()
    }
    return returned
  }
}
