import { eng_components } from "../../locales/components/en"
import { pt_components } from "../../locales/components/pt-BR"

export interface Translations {
  [key: string]: string
}

export default class I18n {
  public static readonly translations_set = {
    en: {
      components: eng_components,
      pages: {}
    },
    "pt-BR": {
      components: pt_components,
      pages: {}
    }
  }

  public static translations() {
    return I18n.translations_set
  }

  public static fetch_translation(
    lang: string,
    pieces: "components" | "pages",
    piece_name: string
  ): Translations {
    return I18n.translations()[lang][pieces][piece_name]
  }
}
