import { renderFile } from "ejs"
import { exec } from "child_process"
import { readFile, writeFile } from "fs/promises"
import { readFileSync, writeFileSync } from "fs"

export default class FileManager {
  public readonly texFile: string
  public readonly pdfFile: string
  public readonly templatePath: string

  constructor(
    public readonly fileName: string,
    public readonly lang: string,
    public readonly data: any
  ) {
    const basePath = process.cwd()
    const fileLang = `${fileName}.${lang}`
    this.templatePath = `${basePath}/template/${fileLang}.tex.ejs`
    this.pdfFile = `${basePath}/public/${fileLang}.pdf`
    this.texFile = `${basePath}/temp/${fileLang}.tex`
  }

  fetchPDF() {
    return readFileSync(this.pdfFile)
  }

  createFile() {
    renderFile(this.templatePath, this.data, {
      delimiter: "%"
    })
      .then(file => {
        this.writeTexFile(file)
      })
      .catch(err => {
        console.log(err)
      })
  }

  writeTexFile(file: string) {
    writeFileSync(this.texFile, file, "utf-8")
    this.compileTexFile()
  }

  compileTexFile() {
    exec("cd temp/;make;make clean;cd ..", {
      encoding: "utf-8"
    })
  }
}
