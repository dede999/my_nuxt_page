import "../styles/globals.sass"
import { AppProps } from "next/app"
import { ChakraProvider } from "@chakra-ui/react"
import pageTheme from "../styles/theme"

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ChakraProvider theme={pageTheme}>
      <Component {...pageProps} />
    </ChakraProvider>
  )
}

export default MyApp
