import { NextApiRequest, NextApiResponse } from "next"
import FileManager from "../../../../modules/file_manager"

export default (req: NextApiRequest, res: NextApiResponse) => {
  const { query } = req
  const lang = query.lang as string

  const doc = new FileManager("hello", lang, {
    name: "Amélia",
    signature: "André"
  })

  if (req.method == "GET") {
    res.send(doc.fetchPDF())
  } else {
    doc.createFile()
    res.send({ message: "File has been created" })
  }
}
// Adapt the Hello World example

// After creating the FileManager module, it was about time to place it in the
