import { NextApiRequest, NextApiResponse } from "next"
import { Programming } from "../../../models/programming_languages/prog.model"

export default async function handler(
  req: NextApiRequest,
  resp: NextApiResponse
) {
  const { method, query, body: data } = req
  const id = +(query.id as string)

  await Programming.request("instance", method, { data, id })
    .then(lang => {
      resp.send(lang)
    })
    .catch(err => {
      resp.status(422)
      resp.send(err)
    })
}
