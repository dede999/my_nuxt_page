import { NextApiRequest, NextApiResponse } from "next"
import { Programming } from "../../../models/programming_languages/prog.model"

export default async function handler(
  req: NextApiRequest,
  resp: NextApiResponse
) {
  const { method, body: data } = req

  await Programming.request("collection", method, { data })
    .then(lang => {
      resp.send(lang)
    })
    .catch(err => {
      resp.status(422)
      resp.send(err)
    })
}
