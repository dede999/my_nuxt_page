import Head from "next/head"
import Header from "../components/Header"
import Experiences from "../components/Experiences"
import Presentation from "../components/Presentation"
import Projects from "../components/Projects"

export default function Home() {
  return (
    <div>
      <Head>
        <title>André Luiz -- Desenvolvedor Web 💻 </title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <Presentation />
      <Experiences />
      <Projects />
    </div>
  )
}
