CREATE TABLE IF NOT EXISTS Programming_Languages (
  id 		serial 			primary key,
  name 	varchar(10)	not null 	unique,
  level integer			not null 	default 1 CHECK (level > 0 and level < 20)
);

CREATE TABLE IF NOT EXISTS Tools (
  lang_id	integer,
  name 		varchar(20),
  PRIMARY KEY (lang_id, name),
  FOREIGN KEY (lang_id)
  REFERENCES Programming_Languages(id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
)
