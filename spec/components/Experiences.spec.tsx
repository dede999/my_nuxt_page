import React from "react"
import { shallow, ShallowWrapper } from "enzyme"
import Experiences from "../../components/Experiences"
import { Center, Heading, SimpleGrid, Text } from "@chakra-ui/react"
import I18n from "../../models/support/i18n"

const locale = "some-locale"
jest.mock("next/router", () => ({
  useRouter: jest.fn().mockReturnValue({ locale: locale })
}))

describe("Experiences component", () => {
  let rendered: ShallowWrapper
  let translate = {
    title: "title",
    description: "this is the description",
    link: "https://link.com"
  }

  beforeEach(() => {
    I18n.fetch_translation = jest.fn().mockReturnValue(translate)

    rendered = shallow(<Experiences />)
  })

  describe("Translations", () => {
    it("is expected to have fetched the components translation", () => {
      expect(I18n.fetch_translation).toHaveBeenCalledWith(
        locale,
        "components",
        "experiences"
      )
    })

    it.each(Object.keys(translate))(
      'it is expected to use the "%s" translation key',
      key => {
        expect(rendered.html()).toMatch(translate[key])
      }
    )
  })

  describe("Checking external component call", () => {
    it.each`
      name            | component
      ${"Center"}     | ${Center}
      ${"Heading"}    | ${Heading}
      ${"SimpleGrid"} | ${SimpleGrid}
      ${"Text"}       | ${Text}
    `("is expected to have called $name component", ({ component }) => {
      expect(rendered.exists(component)).toBeTruthy()
    })
  })

  it("is expected to render the component", () => {
    expect(rendered.exists()).toBeTruthy()
  })

  describe("#formatData", () => {
    it("is expected to format a data value", () => {
      expect(rendered.html()).toMatch(/\d{1,2}\-\d{1,2}\-\d{4}/)
    })
  })
})
