import React from "react"
import Header from "../../components/Header"
import { shallow, ShallowWrapper } from "enzyme"

describe("render Navigator", () => {
  let rendered: ShallowWrapper

  beforeEach(() => {
    rendered = shallow(<Header />)
  })

  it("is expected to render the dev's name", () => {
    expect(rendered.find(".devName")).toHaveLength(1)
  })
})
