import React from "react"
import { shallow, ShallowWrapper } from "enzyme"
import Presentation from "../../components/Presentation"
import {
  SimpleGrid,
  Image,
  Heading,
  Text,
  Container,
  Center
} from "@chakra-ui/react"
import I18n from "../../models/support/i18n"

const locale = "some-locale"
jest.mock("next/router", () => ({
  useRouter: jest.fn().mockReturnValue({ locale: locale })
}))

describe("Presentation component", () => {
  let rendered: ShallowWrapper
  let translate = {
    title: "title",
    text: "this is text"
  }

  beforeEach(() => {
    I18n.fetch_translation = jest.fn().mockReturnValue(translate)

    rendered = shallow(<Presentation />)
  })

  describe("Translations", () => {
    it("is expected to have fetched the components translation", () => {
      expect(I18n.fetch_translation).toHaveBeenCalledWith(
        locale,
        "components",
        "presentation"
      )
    })

    it.each(Object.keys(translate))(
      'it is expected to use the "%s" translation key',
      key => {
        expect(rendered.html()).toMatch(translate[key])
      }
    )
  })

  describe("Checking external component call", () => {
    it.each`
      name            | component
      ${"Center"}     | ${Center}
      ${"Container"}  | ${Container}
      ${"Heading"}    | ${Heading}
      ${"Image"}      | ${Image}
      ${"SimpleGrid"} | ${SimpleGrid}
      ${"Text"}       | ${Text}
    `("is expected to have called $name component", ({ component }) => {
      expect(rendered.exists(component)).toBeTruthy()
    })
  })

  it("is expected to render the component", () => {
    expect(rendered.exists()).toBeTruthy()
  })
})
