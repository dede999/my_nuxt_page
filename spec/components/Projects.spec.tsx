import React from "react"
import { FaVuejs } from "react-icons/fa"
import I18n from "../../models/support/i18n"
import { shallow, ShallowWrapper } from "enzyme"
import Projects from "../../components/Projects"
import { SiTypescript, SiReact } from "react-icons/si"
import { DiNodejsSmall, DiRubyRough, DiRor } from "react-icons/di"
import { Center, Heading, SimpleGrid, Text } from "@chakra-ui/react"

const locale = "some-locale"
jest.mock("next/router", () => ({
  useRouter: jest.fn().mockReturnValue({ locale: locale })
}))

describe("render Navigator", () => {
  let rendered: ShallowWrapper
  let translate = {
    title: "title",
    text: "this is text",
    link: "https://link.com"
  }

  beforeEach(() => {
    I18n.fetch_translation = jest.fn().mockReturnValue(translate)

    rendered = shallow(<Projects />)
  })

  describe("Checking external component call", () => {
    it.each`
      name               | component
      ${"Center"}        | ${Center}
      ${"FaVuejs"}       | ${FaVuejs}
      ${"SiTypescript"}  | ${SiTypescript}
      ${"SiReact"}       | ${SiReact}
      ${"DiNodejsSmall"} | ${DiNodejsSmall}
      ${"DiRubyRough"}   | ${DiRubyRough}
      ${"DiRor"}         | ${DiRor}
      ${"Heading"}       | ${Heading}
      ${"SimpleGrid"}    | ${SimpleGrid}
      ${"Text"}          | ${Text}
    `("is expected to have called $name component", ({ component }) => {
      expect(rendered.exists(component)).toBeTruthy()
    })
  })

  describe("Translations", () => {
    it("is expected to have fetched the components translation", () => {
      expect(I18n.fetch_translation).toHaveBeenCalledWith(
        locale,
        "components",
        "projects"
      )
    })

    it.each(Object.keys(translate))(
      'it is expected to use the "%s" translation key',
      key => {
        expect(rendered.html()).toMatch(translate[key])
      }
    )
  })

  it("is expected to render the dev's name", () => {
    expect(rendered.exists()).toBeTruthy()
  })
})
