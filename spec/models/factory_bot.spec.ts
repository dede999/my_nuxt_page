import FactoryBot from "../../models/support/factory_bot"
import { integer } from "casual"

const general_answer = 42
const factory_function = () => general_answer

describe("FactoryBot", () => {
  const factory_bot = new FactoryBot(factory_function)

  describe("# getOneInstance", () => {
    it("is expected to show the factory_function return value", () => {
      expect(factory_bot.getOneInstance()).toEqual(general_answer)
    })
  })

  describe("# getInstances", () => {
    const arrayLength = integer(1, 10)
    const functionReturn = factory_bot.getInstances(arrayLength)

    it("is expected to return an made of the factory_function return values", () => {
      expect(functionReturn.every(el => el === general_answer)).toBeTruthy()
    })

    it("is expected to return an one element array if the quantity argument is not passed", () => {
      expect(factory_bot.getInstances()).toEqual([general_answer])
    })
  })
})
