import I18n from "../../models/support/i18n"

describe("Translation class", () => {
  const first = { key1: "1" }
  const second = { key2: "2" }

  describe("# translations", () => {
    it("is expected to return the translations set", () => {
      expect(I18n.translations()).toEqual(I18n.translations_set)
    })
  })

  describe("# fetch_translation", () => {
    beforeAll(() => {
      I18n.translations = jest.fn().mockReturnValue({
        locale: {
          components: { first },
          pages: { second }
        }
      })
    })

    it.each`
      piece           | piece_name  | expected
      ${"components"} | ${"first"}  | ${first}
      ${"pages"}      | ${"second"} | ${second}
    `(
      "it is expected to return the expected key value pair for $piece_name $piece",
      ({ piece, piece_name, expected }) => {
        expect(I18n.fetch_translation("locale", piece, piece_name)).toEqual(
          expected
        )
      }
    )
  })
})
