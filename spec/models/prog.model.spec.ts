import { Programming } from "../../models/programming_languages/prog.model"
import { word, array_of_words, integer } from "casual"
import FactoryBot from "../../models/support/factory_bot"

const { fn } = jest

const factory = new FactoryBot(() => {
  return {
    name: word,
    level: integer(0, 5),
    frameworks: array_of_words(5)
  }
})
const language = factory.getOneInstance()
const languages = factory.getInstances(2)

describe("Programming Language Model", () => {
  const deletedTuples = integer(0)

  describe("Class components", () => {
    describe("repository", () => {
      it("is expected to have a repository object", () => {
        expect(Programming.repository).toBeDefined()
      })
    })

    describe("collection methods", () => {
      beforeAll(() => {
        Programming.repository.create = fn().mockResolvedValue(language)
        Programming.repository.findMany = fn().mockResolvedValue(languages)
      })

      it("is expected to run repository.findMany", async () => {
        expect(await Programming.request("collection", "GET", {})).toEqual(
          languages
        )
      })

      it("is expected to run repository.create", async () => {
        expect(await Programming.request("collection", "POST", {})).toEqual(
          language
        )
      })
    })

    describe("instance methods", () => {
      beforeAll(() => {
        Programming.repository.findUnique = fn().mockResolvedValue(language)
        Programming.repository.update = fn().mockResolvedValue(language)
        Programming.repository.deleteMany = fn().mockResolvedValue({
          count: deletedTuples
        })
      })

      it.each`
        http_method | db_method       | expectation
        ${"GET"}    | ${"findUnique"} | ${language}
        ${"PUT"}    | ${"upload"}     | ${language}
        ${"DELETE"} | ${"deleteMany"} | ${{ count: deletedTuples }}
      `(
        "is expected to run repository.$db_method",
        async ({ http_method, expectation }) => {
          expect(
            await Programming.request("instance", http_method, {})
          ).toEqual(expectation)
        }
      )
    })
  })
})
