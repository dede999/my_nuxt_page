import * as child_process from "child_process"
import * as fs from "fs"
import * as ejs from "ejs"
import FileManager from "../../modules/file_manager"

const renderedTex = "LaTeX file"
const error = "Error"
const fileBuffer = "Read my bits"

jest.mock("ejs", () => ({
  renderFile: jest
    .fn()
    .mockResolvedValueOnce(renderedTex)
    .mockRejectedValueOnce(error)
}))

jest.mock("fs", () => ({
  readFileSync: jest.fn().mockReturnValue(fileBuffer),
  writeFileSync: jest.fn()
}))

jest.mock("child_process", () => ({
  exec: jest.fn()
}))

describe("FileManager", () => {
  const cwd = "current-working-directory"
  const name = "File name"
  const lang = "fake-one"
  const data = { key: 1 }
  let sample: FileManager

  describe("constructor", () => {
    beforeEach(() => {
      process.cwd = jest.fn().mockReturnValue(cwd)
      sample = new FileManager(name, lang, data)
    })

    it("is expected to have queried the current working directory", () => {
      expect(process.cwd).toHaveBeenCalled()
    })

    it.each`
      attribute         | value
      ${"templatePath"} | ${`${cwd}/template/${name}.${lang}.tex.ejs`}
      ${"pdfFile"}      | ${`${cwd}/public/${name}.${lang}.pdf`}
      ${"texFile"}      | ${`${cwd}/temp/${name}.${lang}.tex`}
    `(
      "is expected to have correctly set $attribute with $value",
      ({ attribute, value }) => {
        expect(sample[attribute]).toEqual(value)
      }
    )
  })

  describe("fetchPDF", () => {
    let result
    sample = new FileManager(name, lang, data)

    beforeEach(() => {
      jest.spyOn(fs, "readFileSync")

      result = sample.fetchPDF()
    })

    it("is expected to return a file buffer", () => {
      expect(result).toEqual(fileBuffer)
    })

    it("is expected to have synchronously read the file", () => {
      expect(fs.readFileSync).toHaveBeenCalledWith(sample.pdfFile)
    })
  })

  describe("createFile", () => {
    beforeEach(() => {
      jest.spyOn(console, "log")
      jest.spyOn(sample, "writeTexFile")

      sample.createFile()
    })

    it("is expected to have written tex file with buffer", () => {
      expect(sample.writeTexFile).toHaveBeenCalledWith(renderedTex)
    })

    it("is expected to have logged the error", () => {
      expect(console.log).toHaveBeenCalledWith(error)
    })
  })

  describe("writeTexFile", () => {
    beforeEach(() => {
      jest.spyOn(fs, "writeFileSync")
      jest.spyOn(sample, "compileTexFile")

      sample.writeTexFile(fileBuffer)
    })

    it("is expected to have written tex file with buffer", () => {
      expect(fs.writeFileSync).toHaveBeenCalledWith(
        sample.texFile,
        fileBuffer,
        "utf-8"
      )
    })

    it("is expected to have compiled the tex file", () => {
      expect(sample.compileTexFile).toHaveBeenCalled()
    })
  })

  describe("compileTexFile", () => {
    beforeEach(() => {
      jest.spyOn(child_process, "exec")

      sample.compileTexFile()
    })

    it("is expected to have ran commands in the Makefile", () => {
      expect(child_process.exec).toHaveBeenCalledWith(
        "cd temp/;make;make clean;cd ..",
        {
          encoding: "utf-8"
        }
      )
    })
  })
})
