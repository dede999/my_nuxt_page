import React from "react"
import Home from "../../pages"
import Header from "../../components/Header"
import Presentation from "../../components/Presentation"
import { shallow } from "enzyme"

describe("Index page", () => {
  let rendered

  beforeEach(() => {
    rendered = shallow(<Home />)
  })

  it("is expected to render the component", function () {
    expect(rendered.exists()).toBeTruthy()
  })

  it.each`
    name              | component
    ${"Header"}       | ${Header}
    ${"Presentation"} | ${Presentation}
  `("is expected to have called $name component", ({ component }) => {
    expect(rendered.exists(component)).toBeTruthy()
  })
})
