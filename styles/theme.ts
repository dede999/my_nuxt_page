import { extendTheme } from "@chakra-ui/react"

const pageTheme = extendTheme({
  components: {
    Heading: {
      baseStyle: {
        fontFamily: "Trocchi"
      }
    }
  }
})

export default pageTheme
